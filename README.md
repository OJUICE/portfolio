# Portfolio

### My Java Notes

2019-03-09

Es gibt 3 access modifiers in Java:
* private: Es nur abrufbar in der Class
* default: Es ist abrufbar nur im Package
* public: Es ist ueberall im Project zugaenglich


### My linux notes:

```
9-2-2018
to check directory - pwd
to change directory - cd
to create create directory - mkdir
to open text editor - nano
to view file and keep content on screen - more
to view file and discard content - less
to exit less - q
to show history - history
to clear line - ctrl+c
to go to beginning of line - ctrl+a
to the end - ctrl+e

============
GIT
============
how to install git - apt-get install git-core
initialize git - git init
select which file to version - git add
quick save (almost like ctrl+s in notepad) - git commit
Save As... - git branch
open the vesion you just saved (git branch) switch between branches - git checkout
git branch + git checkout - git checkout -b
delete old version - git branch -D
list all versions - git branch
if you made a mistake - git reset --hard
see modification history of gits - git log
add comment in line - git commit -m


==========
basic linux commands
=========
search in history for specific word - history | grep 'sql'

==========
sharing terminal in ubuntu
============
ssh oliver@51.68.190.223
zienkiewicz
screen -S shared
Ctrl+a
:multiuser on
Ctrl + A
:acladd lukasz

In order to share a screen in Ubuntu:
ssh oliver@51.68.190.223
password: zienkiewicz
screen -S shared
Ctrl + A
:multiuser on
Ctrl + A
:acladd lukasz
to end: screen -X -S oliver quit

============================================
2018-10-20

in order to share a terminal in Ubuntu:
screen -S shared
Ctrl + A
:multiuser on
Ctrl + A
:acladd lukasz

and wait for response. Lukasz should type: screen -x lukasz/shared


1. To change a user password type: passwd user e.g. (passwd oliver)
2. to run a command as an admin type: sudo .... e.g. (sudo apt-get update)
3. To install GIT type: sudo apt-get install git-core
4. to move a folder or file into another one type: mv [source] [destination]
5. in order to to change a directory type:
         a. cd directoryname
         b. cd / brings to root directory
         c. cd - brings to /home/oliver
         d. cd .. - bring you one level up
6. in order to create a file just type nano filename
7. in order to create a git repository type: git init
8. alias edit_my_log='nano /home/oliver/workspace/my_log'
======
Environment Variable
=====

name_of_var=value e.g. ( MY_ADDRESS=BROOKLYN)
in or oder to view value type: echo $MY_ADDRESS
NEXT_VER=1.0.4
alias SAVE='git add . && git commit -m "default message" && git checkout -b $NEXT_VER'

*******************
2018-10-21

wie kann man ein ssh key generieren?
-> https://www.ssh.com/ssh/copy-id
ssh-keygen
pfad: ~/.ssh/id_rsa

um einen schluessel zu dem server zu kopieren:
ssh-copy-id -i ~/.ssh/id_rsa oliver@51.68.190.223

wie kann man die datei kopieren?
cp quellepfad zielpfad
durch das netzwerk:
scp quellepfad serverip:zielpfad
 zb. scp /home/oliwer/handbuch oliver@51.68.190.223:/home/oliver/workspace/handbuch

wie kann man dateinamen aendern?
mv was wie
z.b. mv /home/oliver/workspace /home/oliver/okt18
z.b. mv workspace okt18

wie kann man eine datei verschieben?
mv /home/oliver/workspace/datei /home/oliver/workspace/okt18/datei
mv datei okt18/datei
mv /home/oliver/workspace/datei /home/oliver/datei
mv datei ../datei
mv /home/oliver/workspace/datei /home/datei
mv datei ../../datei
mv /home/oliver/workspace/datei /datei
mv datei ../../../datei
mv datei datei_backup

nutzung des werkzeuges GREP:
grep  'oliver' my_log
history | grep 'oliver'

wie kann man ordener oder datei finden?
find ~ -name '*oli*'  (wie SQL wildcards)
find ~ -name oliver

grep -rnw /home/oliver/workspace/oct18/logs/ -e "Exception during transaction"

===============================================
        Wed Oct 24 14:12:25 CEST 2018
===============================================

date
date >> /home/oliver/workspace/oct18/my_log

How to append a new line:
echo >> /home/oliver/workspace/oct18/my_log

How to append text:
echo 'something'
echo 'something' >> /home/oliver/workspace/oct18/my_log

Fri Oct 26 23:47:57 CEST 2018
GIT RESET SOFT/MIXED/HARD
- [ ] - any change |    can be undone with git reset --hard
- [ ] - git add .  |    can be undone with git reset --mixed
- [ ] - git commit |    can be undone with git reset --soft HEAD~

Sat Oct 27 15:09:36 CEST 2018
==================================

um etwas zu finden und es zu konkatinieren kann man awk verwenden z.b.
 find logs/ -name *gc.log*

jetzt kann man mit AWK verwenden um es zu konkatinieren:
find logs/ -name '*gc.log*' | awk -F\, '{print "prefix " $1 " sufix"}'

um etwas in ein script einzufuegen und danach es zu aktivieren:
find logs/ -name '*r*' | awk -F\, '{print "cp " $1 " test003/"}' > copy.sh

find logs/ -name '*r*' | awk -F\, '{print "cp " $1 " test003/ printf "copied successfully!""}' > copy.sh

leider " kann man nicht einfach schreiben in awk, also muss man \x22 als anfuehrungszeichen verwenden. Das beispiel oben
 ist falsch aber das unten ist richtig.

find logs/ -name '*r*' | awk -F\, '{print "cp " $1 " test003/ \n printf \x22 copied " $1 " s
uccessfully!\x22\n"}' > copy.sh

-> um  das skript auszufuehren:
sh ./copy.sh


Sun Oct 28 15:37:38 CET 2018
===================================
wie kann man zwei befehle einen ausfuehren zb.:
git add . && git commit -m 'message' && git checkout -b ver.1.1.1

wie kann man alle ordner finden die den gesuchten text enthalten:
grep -rnw '/home/oliver/workspace/oct18/logs' -e "Exception"

Um mehr ueber die befehle zu lesen muss 'man' als prefix geschrieben werden + befehl
man grep
man find
man git
man history
man cp

Übung:
1. Erstell eine Skript Datei welche alle dateien kopiert mit dem name R drinnen
find logs/ -name '*r*' | awk -F\, '{print "cp " $1 " test003/ \n printf \x22 copied " $1 " s
uccessfully!\x22\n"}' > copy.sh

2. Erstell eine Skript Datei welche alle dateien kopiert mit dem inhalt 'Exception'

grep -rnw /home/oliver/workspace/oct18/logs/ -e "Exception" -c | awk -F\, '{print "cp " $1 " test003/ \n printf \x22 copied " $1 " s
uccessfully!\x22\n"}' > copy2.sh

um rechte zu aendern kann man read,write und execution rechte einfuehren
zb. chmod 777 zieldatei
    chmod 000 zieldatei (keine rechte)

um eine neue datei zu generieren:
touch neuedatei
echo inhalt >> neuedatei
nano neuedatei

Um 2 dateien zusammenzufuegen:
cat erstedatei zweitedatei > neuedatei

Wed Oct 31 15:33:26 CET 2018
====================================
less /home/oliver/workspace/oct18/my_log
tail /home/oliver/workspace/oct18/my_log

Task:
create a symbolic link in a new folder to oct18
mv this link

nano /home/oliver/workspace/oct18/my_log
in order to navigate to the end:
Ctrl + _
Ctrl + v

clean your directory, delete logs folder and commit


to clone your repo:
ssh-copy-id -i ~/.ssh/id_rsa.pub git@51.68.190.223
password: berlin2018
git clone git@51.68.190.223:/home/git/workspace/oct18.git

head- give you the first 5 rows of the file. with -n xx as number you can define the line numbers

tail- last 5 rows of text document
cat- concatinates the documents e.g. cat doc1 doc2 >> doc3

===============================================
===============================================

Sat Nov 17 15:05:18 CET 2018

sh steht fuer SHELL
shell ist nur ein programm und es startet mit einem betriebssystem am anfang
shell ist ein synonym fuer ein Command Line Interface (CLI)
ein antonym/gegenwort zu CLI ist Graphic User Interface (GUI)
Windows hat immer GUI da es graphisch aufgebaut ist
Es gibt auch CLI in Windows das heisst PowerShell
standard shell in linux heisst bash

Es gibt viele variationen von shell in linux:
-bash
-kornshell
-c shell
-z shell (zsh)

Es gibt viele variationen von  shell in windows:
-MS Dos
-Powershell
-Putty
-Gitbash

Um einen alias hinzuzufuegen muss man eine besondere shell datei editieren.
fuer Bash: ~/.bashrc
fuer ZSH:  ~/.zshrc

Um eine environment variable global abzurufen muss export davor stehen z.b.
export first_num=1

Um einen Befehl Output in eine Variable zu speichern muss man die folgende Syntax verwenden:
variable=$(command) z.b. current_version=$(head /home/oliver/workspace/nov18/current_version)

Um eine aritmetrische/matematische Operationen durchzuführen muss man die folgende Syntax benutzen:
num=$((variable1+value)) z.b next_version=$((current_version+1))

ALIAS um automatisch neues git zu starten und abspeichern:
alias quick_save='cd /home/oliver/workspace/nov18/ && git add . && git commit && /home/oliver/workspace/nov18/increment.sh'
FIX: alias quick_save='/home/oliver/workspace/nov18/quicksave.sh'

SYMBOLIC LINKS

- Windows               | Linux                 | Programming Language  | DB                                    | Internet
- =============================================================================================================================
- shortcut              | symbolic link         | pointer               | any value of the same type            | hyperlink
- does not exist        | hard link             | does not exist        | foreign key                           | does not exist
- secondary memory      | secondary memory      | main memory (RAM)     | any Unique or Primary Key(which is    | HTTP URL
                                                                        | unique by default)
ln -s /home/oliver/workspace/nov18/my_log link_to_my_log

HOW TO INSTALL JAVA
sudo apt update
sudo apt install default-jdk
java -version

echo $JAVA_HOME
sudo update-alternatives --config java

nano TheJavaFile.java
javac TheJavaFile.java && java TheJavaFile oliver

class TheJavaFile {

    static void main(String[] args) {
        doSomething(args);
    }

    static void doSomething(String[] args) {
        for (int i = 0; i < args.length; i++) {
            System.out.println("Argument " + i + ": " + args[i].toUpperCase());
        }
    }
}

===============================================
===============================================

Sun Nov 18 15:26:38 CET 2018

How to compile and and run java app:

cd /home/oliver/workspace/nov18/project01/src && javac com/company/Main.java
cd /home/oliver/workspace/nov18/project01/src && java com.company.Main abc sonntag bohrmaschine
```


